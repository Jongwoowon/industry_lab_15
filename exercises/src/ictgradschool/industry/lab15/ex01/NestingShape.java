package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwon117 on 9/05/2017.
 */
public class NestingShape extends Shape {

    private List<Shape> _children = new ArrayList<>();

    public NestingShape() {
        this(DEFAULT_X_POS, DEFAULT_Y_POS, DEFAULT_DELTA_X, DEFAULT_DELTA_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public NestingShape(int x, int y) {
        this(x, y, DEFAULT_DELTA_X, DEFAULT_DELTA_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        this(x, y, deltaX, deltaY, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (Shape children : _children) {
            children.move(this.fWidth, this.fHeight);
        }
    }

    public void paint(Painter painter) {
        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);
        for (Shape children : _children) {
            children.paint(painter);
        }
        painter.translate(-fX, -fY);
    }

//    Attempts to add a Shape to a NestingShape object. If successful, a two-way link is
//    established between the NestingShape and the newly added Shape. This method throws an
//    IllegalArgumentException if an attempt is made to add a Shape to a NestingShape
//    instance where the Shape argument is already a child within a NestingShape instance. An
//    IllegalArgumentException is also thrown when an attempt is made to add a Shape that
//    will not fit within the bounds of the proposed NestingShape object.

    public void add(Shape child) throws IllegalArgumentException {
//                (Shape children : _children){
        if (child.parent != null){
            throw new IllegalArgumentException("This shape already has a parent...");
        }
        else if (child.getWidth() + child.getX() > this.getWidth() || child.getHeight() + child.getY() > this.getHeight()){
            throw new IllegalArgumentException("This doesnt fit within the proposed NestingShape object!");
        }
        else{
            _children.add(child);
            child.setParent(this);
        }
    }

    public void remove(Shape child) {
        if (_children.contains(child)) {
            _children.remove(child);
            child.setParent(null);
        }
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > _children.size()-1) {
            throw new IndexOutOfBoundsException("Index out of bounds exception thrown!");
        }
        return _children.get(index);
    }


    public int shapeCount() {
        return _children.size();
    }

    public int indexOf(Shape child) {
        if (_children.contains(child)) {
            return _children.indexOf(child);
        }
        return -1;
    }

    public boolean contains(Shape child) {
        if (_children.contains(child)) {
            return true;
        }
        return false;
    }
}
