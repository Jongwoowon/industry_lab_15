package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements the TreeModel interface by delegating to Directory
 * and File objects that form a hierarchical representation of a filestore.
 * This class is an object adapter.
 *
 */
public class TreeModelAdapter implements TreeModel {

	private NestingShape _adaptee;

	private List<TreeModelListener> _treeModelListeners;

	/**
	 * Creates a TreeModelAdapter object with the adaptee parameter, root.
	 */
	public TreeModelAdapter(NestingShape _adaptee) {
		this._adaptee = _adaptee;
		_treeModelListeners = new ArrayList<TreeModelListener>();
	}

	/**
	 * Returns a reference to the root of the filestore.
	 */
	public Object getRoot() {
		return _adaptee;
	}

	/**
	 * Returns the number of children (File/Directory objects) parameter parent
	 * has. parent is expected to point to a Directory object contained within 
	 * the adaptee (root) Directory.
	 */
	public int getChildCount(Object parent) {
		int result = 0;
		Shape shape = (Shape) parent;

		if (shape instanceof NestingShape) {
			NestingShape nestingShape = (NestingShape) shape;
			result = nestingShape.shapeCount();
		}
		return result;
	}

	/**
	 * Returns true if node refers to a Directory object, false otherwise.
	 */
	public boolean isLeaf(Object node) {
		return !(node instanceof NestingShape);
	}

	/**
	 * Adds the TreeModelListener parameter to this TreeModel's list of 
	 * listeners.
	 */
	public void addTreeModelListener(TreeModelListener l) {
		_treeModelListeners.add(l);
	}

	/**
	 * Removes the TreeModelListener parameter from this TreeModel's list of
	 * listeners.
	 */
	public void removeTreeModelListener(TreeModelListener l) {
		_treeModelListeners.remove(l);
	}

	/**
	 * Returns child File/Directory object that is stored within the parent
	 * parameter at the position specified by the index parameter.
	 */
	public Object getChild(Object parent, int index) {
		Object result = null;

		if (parent instanceof NestingShape) {
			NestingShape dir = (NestingShape) parent;
			result = dir.shapeAt(index);
		}
		return result;
	}

	/**
	 * Returns the index position at which the child File/Directory parameter
	 * is stored within the Directory object that parameter parent is expected 
	 * to refer. This method returns -1 if the object pointed to by the child
	 * parameter is not actually a child of parent.
	 */
	public int getIndexOfChild(Object parent, Object child) {
		int indexOfChild = -1;

		if (parent instanceof NestingShape) {
			NestingShape nest = (NestingShape) parent;
			indexOfChild = nest.indexOf((Shape) child);
		}
		return indexOfChild;
	}

	public void valueForPathChanged(TreePath path, Object newValue) {
		/* No implementation required. */
	}
}